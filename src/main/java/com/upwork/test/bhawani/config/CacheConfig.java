package com.upwork.test.bhawani.config;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@EnableCaching
public class CacheConfig {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    private static final long HEAP = 10000000L;
    private static final long TTL = 1000000L;
    public CacheConfig() {
        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                        ResourcePoolsBuilder.heap(HEAP))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(TTL)))
                        .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(CacheConstants.ADD, jcacheConfiguration);
            cm.createCache(CacheConstants.MUL, jcacheConfiguration);
            cm.createCache(CacheConstants.SUB, jcacheConfiguration);
            cm.createCache(CacheConstants.DIVIDE, jcacheConfiguration);
            cm.createCache(CacheConstants.MODULUS, jcacheConfiguration);
            cm.createCache(CacheConstants.POW, jcacheConfiguration);
            cm.createCache(CacheConstants.SQRT, jcacheConfiguration);
            cm.createCache(CacheConstants.CBRT, jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
