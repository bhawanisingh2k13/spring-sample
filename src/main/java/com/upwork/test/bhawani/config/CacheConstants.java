package com.upwork.test.bhawani.config;

public class CacheConstants {

    public static final String ADD = "addition";
    public static final String SUB = "subtraction";
    public static final String MUL = "multiplication";
    public static final String DIVIDE = "division";
    public static final String MODULUS = "modulus";
    public static final String POW = "power";
    public static final String SQRT = "root.square";
    public static final String CBRT = "root.cube";
}
