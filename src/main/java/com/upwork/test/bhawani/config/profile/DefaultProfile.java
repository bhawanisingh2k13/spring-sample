package com.upwork.test.bhawani.config.profile;

import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

public final class DefaultProfile {

    private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

    private DefaultProfile() {
    }


    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        defProperties.put(SPRING_PROFILE_DEFAULT, Profiles.DEV);
        app.setDefaultProperties(defProperties);
    }


    public static String[] getActiveProfiles(Environment env) {
        String[] profiles = env.getActiveProfiles();
        if (profiles.length == 0) {
            return env.getDefaultProfiles();
        }
        return profiles;
    }
}