package com.upwork.test.bhawani.config.profile;

public class Profiles {
    public static final String DEV = "dev";
    public static final String SWAGGER = "swagger";
    public static final String PROD = "prod";
}
