package com.upwork.test.bhawani.resource;

import com.upwork.test.bhawani.model.ResultDTO;
import com.upwork.test.bhawani.service.MathService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/upwork/test")
public class MathResource {

    private final MathService mathService;

    public MathResource(MathService mathService) {
        this.mathService = mathService;
    }

    /**
     * {@code GET/upwork/test/add/:first/:second} : add first and second
     *
     * @param first the first number
     * @param second the second number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/add/{first}/{second}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> add(@PathVariable double first, @PathVariable double second) {
        double result =  mathService.add(first, second);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/add/:first/:second/:third} : add first, second and third
     *
     * @param first the first number
     * @param second the second number
     * @param third the third number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/add/{first}/{second}/{third}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> add(@PathVariable double first, @PathVariable double second, @PathVariable double third) {
        double result =  mathService.add(first, second, third);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/mul/:first/:second} : multiply first and second
     *
     * @param first the first number
     * @param second the second number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/mul/{first}/{second}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> mul(@PathVariable double first, @PathVariable double second) {
        double result =  mathService.mul(first, second);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/mul/:first/:second/:third} : multiply first, second and third
     *
     * @param first the first number
     * @param second the second number
     * @param third the third number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/mul/{first}/{second}/{third}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> mul(@PathVariable double first, @PathVariable double second, @PathVariable double third) {
        double result =  mathService.mul(first, second, third);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/sub/:first/:second} : subtract second from one
     *
     * @param first the first number
     * @param second the second number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/sub/{first}/{second}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> sub(@PathVariable double first, @PathVariable double second) {
        double result =  mathService.sub(first, second);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/sub/:first/:second/:third} : subtract second and third from first
     *
     * @param first the first number
     * @param second the second number
     * @param third the third number
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if arguments are not valid}.
     */
    @GetMapping(value = "/sub/{first}/{second}/{third}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> sub(@PathVariable double first, @PathVariable double second, @PathVariable double third) {
        double result =  mathService.sub(first, second, third);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/div/:toDivide/:byDivide} : divides toDivide by byDivide
     *
     * @param toDivide the number to be divided
     * @param byDivide the number which divides
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if byDivide is 0}.
     */
    @GetMapping(value = "/div/{toDivide}/{byDivide}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> divide(@PathVariable double toDivide, @PathVariable double byDivide) {
        double result =  mathService.divide(toDivide, byDivide);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/mod/:toMod/:byMod} : mod toMod by byMod
     *
     * @param toMod the number whose mod to be calculated
     * @param byMod the number by which mod to be calculated
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if byMod is 0}.
     */
    @GetMapping(value = "/mod/{toMod}/{byMod}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> modulus(@PathVariable double toMod, @PathVariable double byMod) {
        double result =  mathService.modulus(toMod, byMod);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/pow/:number/:power} : returns number raised to power
     *
     * @param number the number whose power to be raise
     * @param power the value by which power to be raised
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result}.
     */
    @GetMapping(value = "/pow/{number}/{power}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> power(@PathVariable double number, @PathVariable double power) {
        double result =  mathService.power(number, power);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/sqrt/:number} : return the square root of a number
     *
     * @param number the number whose square root to be calculated
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if number is -ve}.
     */
    @GetMapping(value = "/sqrt/{number}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> sqrt(@PathVariable double number) {
        double result =  mathService.sqrt(number);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

    /**
     * {@code GET/upwork/test/cbrt/:number} : return the cube root of a number
     *
     * @param number the number whose cube root to be calculated
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the result, or with status {@code 400 (Bad Request) if number is -ve}.
     */
    @GetMapping(value = "/cbrt/{number}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<ResultDTO> cbrt(@PathVariable double number) {
        double result =  mathService.cbrt(number);
        return new ResponseEntity<>(new ResultDTO(result), HttpStatus.OK);
    }

}
