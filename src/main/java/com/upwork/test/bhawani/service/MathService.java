package com.upwork.test.bhawani.service;

import com.upwork.test.bhawani.config.CacheConstants;
import com.upwork.test.bhawani.error.InsufficentArgumentsException;
import com.upwork.test.bhawani.error.NegativeNumberException;
import com.upwork.test.bhawani.error.ZeroDivideException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MathService {

    @Cacheable(CacheConstants.ADD)
    public double add(Double... numbersToAdd) {
        validate(numbersToAdd);
        return Arrays.stream(numbersToAdd).mapToDouble(Double::doubleValue).sum();
    }

    @Cacheable(CacheConstants.MUL)
    public double mul(Double... numbersToMul) {
        validate(numbersToMul);
        return Arrays.stream(numbersToMul).mapToDouble(Double::doubleValue).reduce(1, (numberOne, numberTwo) -> numberOne * numberTwo);
    }

    @Cacheable(CacheConstants.SUB)
    public double sub(Double... numbersToSub) {

        validate(numbersToSub);
        Double result = numbersToSub[0];

        for (int i = 1; i < numbersToSub.length; i++) {
            result = result - numbersToSub[i];
        }
        return result;
    }

    @Cacheable(CacheConstants.DIVIDE)
    public double divide(double toDivide, double byDivide) {
        if(byDivide == 0 ) {
            throw new ZeroDivideException();
        }
        return toDivide / byDivide;
    }

    @Cacheable(CacheConstants.MODULUS)
    public double modulus(double toMod, double byMod) {
        if(byMod == 0 ) {
            throw new ZeroDivideException();
        }
        return toMod % byMod;
    }

    @Cacheable(CacheConstants.POW)
    public double power(double number, double power) {
        return Math.pow(number, power);
    }

    @Cacheable(CacheConstants.SQRT)
    public double sqrt(double number) {
        if(number < 0 ) {
            throw new NegativeNumberException();
        }
        return Math.sqrt(number);
    }

    @Cacheable(CacheConstants.CBRT)
    public double cbrt(double number) {
        if(number < 0 ) {
            throw new NegativeNumberException();
        }
        return Math.cbrt(number);
    }

    private void validate(Double... numbers) {
        if (numbers.length < 2) {
            throw new InsufficentArgumentsException("Minimum of 2 arguments are required, please refer documentation");
        }
    }
}
