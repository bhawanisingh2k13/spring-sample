package com.upwork.test.bhawani.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class NegativeNumberException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public NegativeNumberException() {
        super(ErrorConstants.NEG_NUM, "Negative Numbers!", Status.BAD_REQUEST, "For roots -ve numbers are not allowed :(");

    }
}
