package com.upwork.test.bhawani.error;

import java.net.URI;

public final class ErrorConstants {

    public static final String ERR_VALIDATION = "error.validation";
    public static final String PROBLEM_BASE_URL = "https://www.upwork.com/bhawani/test/";
    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/error");
    public static final URI DIVIDE_BY_ZERO = URI.create(PROBLEM_BASE_URL + "/divide-by-zero");
    public static final URI NEG_NUM = URI.create(PROBLEM_BASE_URL + "/negative-number");
    public static final URI INSUFFICIENT_PARAM = URI.create(PROBLEM_BASE_URL + "/insufficient-params");

    private ErrorConstants() {
    }
}
