package com.upwork.test.bhawani.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class InsufficentArgumentsException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InsufficentArgumentsException(String message) {
        super(ErrorConstants.INSUFFICIENT_PARAM, "Params Mismatch!", Status.BAD_REQUEST, message);
    }
}
