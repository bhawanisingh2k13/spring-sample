package com.upwork.test.bhawani.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ZeroDivideException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public ZeroDivideException() {
        super(ErrorConstants.DIVIDE_BY_ZERO, "Divide By Zero", Status.BAD_REQUEST, "You are trying to divide by zero, We doesn't support it :(");
    }
}
