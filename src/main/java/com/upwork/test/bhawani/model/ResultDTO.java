package com.upwork.test.bhawani.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ResultDTO {
    private double result;
}
